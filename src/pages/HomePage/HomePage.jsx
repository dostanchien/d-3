
import {useLocation} from  "react-router-dom";
import PropTypes from 'prop-types';
import CardList from "../../components/Card/Components/CardList";

const HomePage = ({favorite, date, handleModal, handleCurrentPost, onClickIcon}) => {
	const location = useLocation() /* useLocation хук нам дає обьект в якому індефикується кожна сторінка   */
	console.log('HomePage location',location);
	return (
		<>
			<section>
			<h2>Керамічні обігрівачі</h2>
			<CardList favorite={favorite} date={date} handleModal={handleModal} handleCurrentPost={handleCurrentPost}
				onClickIcon={onClickIcon}></CardList>
			</section>
		</>
	)
}

HomePage.defaultProps = {
	onClickIcon: () => {},
	handleModal: () => {},
	handleCurrentPost: () => {}
  }
  HomePage.propTypes = {
	  favorite: PropTypes.array,
	  date: PropTypes.array,
	handleModal: PropTypes.func,
	handleCurrentPost: PropTypes.func,
	onClickIcon: PropTypes.func,
	children:PropTypes.any
  }
export default HomePage