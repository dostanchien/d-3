import {Link, useLocation, useNavigate} from "react-router-dom";


import './NotPage.css'
const NotPage = () => {
	const location = useLocation()
	const navigate = useNavigate() /* useNavigate цей хук нам позволяє робити редерект на іншу сторінку за якоюсь умовою або при виконанні якоїсь функції, ще можно зробити переходи по хісторі браузера*/
	console.log('HomePage location',location);

	const goBack = () => navigate(-1) /*  переходи по хісторі браузера, перехід на попередню сторінку */
	const rederect = () => navigate('/user') /*  редерект на сторінку user */
	return (
		<div className="not-page">
			<div>
				<p className="page-title">This page has been abducted.</p>
				<p className="page-desc">Let’s head back home and try that again. The truth is out there…</p>
				<p>
					<Link to="/">Home</Link> {/* Link компонент це наша лінка але вона працює без перезавантаження сторінки.  */}
					<div onClick={goBack}>goBack</div>
					<div onClick={rederect}>rederect</div>
					{/*<a href="/">Home</a>*/}
				</p>
			</div>
		</div>
	)
}

export default NotPage