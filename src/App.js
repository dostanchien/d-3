import React from 'react';
import {Routes, Route} from "react-router-dom";
import { useLocalStorage} from "./helpers/useLocalStorage";
import NavState from './components/menu/context/navState';
import MainMenu from './components/menu/components/MainMenu';
import Footer from './components/Footer/Footer';
import FavoritePage from './pages/FavoritePage/FavoritePage';
import CartPage from './pages/CartPage/CartPage';
import NotPage from './pages/NotPage/NotPage';
import HomePage from './pages/HomePage/HomePage';
import {sendRequest} from "./helpers/sendRequest";
import {useEffect, useState} from "react";
import ModalImage from './components/Modal/ModalImage/ModalImage';
function App() {
	const [productArray, setProductArray] = useState([])
	useEffect(() => {
		sendRequest(window.location.origin +'/products.json')
			.then((date) => {	
				setProductArray(date);
			})
		}, []);

	const [carts, setCart] = useLocalStorage("CookiCart", "");
	const [favorites, setFavorite] = useLocalStorage("CookiFavorite", "");

	const handleFavorites = (products) => {
		const isAdded = favorites.some((favorite)=> favorite.article === products.article)
		if(isAdded){
			setFavorite(favorites.filter((favorite)=> favorite.article !== products.article));
			return
		}
		setFavorite([...favorites,products])
	}

	const handleCart = (products) => {
		const isAdded = carts.some((cart)=> cart.article === products.article)
		isAdded ?
				setCart(carts.filter((cart)=> cart.article !== products.article))
				:
				setCart([...carts,products]);
	}
	const [isModalImage,setIsModalImage] = useState(false)
	const handleModalImage = () => setIsModalImage(!isModalImage)

	const [currentPost, setCurrentPost] = useState({})
	const handleCurrentPost = (cardPost) => setCurrentPost(cardPost)
	
  return (
  <>
  
     <NavState>
      <MainMenu  countFavorite={favorites.length} countCart={carts.length}/>
    </NavState>
	
	<section className="pt-5 p-2 text-center">
	<Routes>
      <Route path="/" element={<HomePage favorite={favorites} date={productArray} handleModal={handleModalImage} handleCurrentPost={handleCurrentPost}
				onClickIcon={handleFavorites}>
			</HomePage>} />
      <Route path="favorite" element={<FavoritePage favorite={favorites} date={favorites} handleModal={handleModalImage} handleCurrentPost={handleCurrentPost}
				onClickIcon={handleFavorites}>
				</FavoritePage>} />
	  <Route path="cart" element={<CartPage favorite={favorites} date={carts} handleModal={handleModalImage} handleCurrentPost={handleCurrentPost}
				onClickIcon={handleFavorites}>
				</CartPage>} />
		<Route path="*" element={<NotPage/>}/>
    </Routes>
	</section>
	<Footer/>
			<ModalImage
                    isOpen={isModalImage}
                    handleClose={()=>{handleModalImage()}}
					products={currentPost}
					cart={carts}
					handleOk={()=> {
						handleCart(currentPost)
					}}
           />
	
   </>
  );
}
export default App;

