import PropTypes from 'prop-types'
import styled from "styled-components"
import CardHeader from './CardHeader';
import CardAjax from './CardAjax';
import CardBody from './CardBody';

const CardSt=styled.div`
position: relative;
display: -ms-flexbox;
display: flex;
-ms-flex-direction: column;
flex-direction: column;
min-width: 0;
word-wrap: break-word;
background-color: #fff;
background-clip: border-box;
border: 1px solid rgba(0, 0, 0, 0.125);
border-radius: 0.25rem;
margin-bottom: 1rem;
-ms-flex: 1 0 0%;
flex: 1 0 0%;
margin-right: 1rem;
margin-bottom: 0;
margin-left: 1rem;
float: left;
width:15rem
`;
const ImgSt =styled.img`
  -ms-flex-negative: 0;
  flex-shrink: 0;
  width: 100%;
  `;
  const SmallSt =styled.p`
  font-size:70%;
  text-align: right;
  font-weight:50;
  `;
  const PriceSt =styled.p`
  font-size:150%;
  text-align: center;
  font-weight:400;
  color:  darkgreen;
  padding-top: 1rem;
  `;
  const ContainerSt= styled.div` 
    position: absolute;
    top: 0;
    right: 0;
    
  `;
  
const CardOrder = ({products,isAdded,handleModal,handleCurrentPost,onClickIcon})=>{

    return(
            <CardSt key={products.article}>
           
               <CardHeader>
                    <figure>              
                        <ImgSt src={products.images} alt={products.name} />
                        <ContainerSt>                            
                       <button onClick={() => {
                                                handleModal()
                                                handleCurrentPost(products)
				                                }} type="button">
                            <svg viewBox="0 0 16 16" width="16" height="16">
                                <path d="m9.3 8 6.1-6.1c.4-.4.4-.9 0-1.3s-.9-.4-1.3 0L8 6.7 1.9.6C1.6.3 1 .3.6.6c-.3.4-.3 1 0 1.3L6.7 8 .6 14.1c-.4.4-.3.9 0 1.3l.1.1c.4.3.9.2 1.2-.1L8 9.3l6.1 6.1c.4.4.9.4 1.3 0s.4-.9 0-1.3L9.3 8z"/>
                            </svg>
                        </button>
                        </ContainerSt>
                        
                    </figure>
                    <CardAjax>{products.color}</CardAjax>
                </CardHeader>
               <CardBody>
                    <SmallSt>Артикул: {products.article}</SmallSt>
                    <h4 className='text-center'>{products.name}</h4>            
                    <PriceSt>{products.price} грн.</PriceSt>
                </CardBody>
            </CardSt>
    )
}

CardOrder.defaultProps = {
    children:'',
    onClickIcon: () => {},
	clickFirst: () => {},
	handleCurrentPost: () => {}

  }
CardOrder.propTypes = {
	isAdded: PropTypes.bool,
	products: PropTypes.object,
    clickFirst: PropTypes.func,
    handleCurrentPost: PropTypes.func,
    onClickIcon: PropTypes.func,
    children:PropTypes.any

}

export default CardOrder