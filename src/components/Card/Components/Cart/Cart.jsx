import {useEffect, useState} from "react";
import PropTypes from 'prop-types'
import {sendRequest} from "../../../../helpers/sendRequest";
import CardList from "../CardList";
import ModalImage from '../../../Modal/ModalImage/ModalImage';
import {ReactComponent as Favorite} from '../../../../icons/favorite.svg';



const Cart = ({cart, favorite, productArray, onClickIcon, onClicAddCart}) => {
    
	return (
		<section className="pt-5 p-2 text-center">
			<h2 className='text-center p-2'>Корзина сайта</h2>
			<CardList favorite={favorite} date={productArray} handleModal={handleModalImage} handleCurrentPost={handleCurrentPost}
				onClickIcon={onClickIcon}><Favorite/>
			</CardList>
		</section>
	)
}
Cart.defaultProps = {
    onClickIcon: () => {},
	onClicAddCart: () => {}
  }
Cart.propTypes = {
    productArray: PropTypes.array,
	cart: PropTypes.array,
	favorite: PropTypes.array,
    onClickIcon: PropTypes.func,
	onClicAddCart: PropTypes.func
}
export default Cart

