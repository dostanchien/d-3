import PropTypes from 'prop-types'
import CardOrder from './CardOrder';

const CardListOrder = ({favorite, date, handleModal,handleCurrentPost,children,onClickIcon})  =>{
   
	const ProductsItems = date.map((item, index) => 
  ( 
  <CardOrder  products={item} key={index}
          isAdded={favorite.some((favor) => favor.article === item.article)}//меняем цвета иконок фаворите=проверка
          handleModal={handleModal}
          handleCurrentPost={handleCurrentPost}
          onClickIcon={onClickIcon}
         >
  </CardOrder>))
  return(    
      <>{ProductsItems}</>
  ) 
}


CardListOrder.defaultProps = {
	onClickIcon: () => {},
	handleModal: () => {},
	handleCurrentPost: () => {}
  }
  CardListOrder.propTypes = {
	  favorite: PropTypes.array,
	  date: PropTypes.array,
	handleModal: PropTypes.func,
	handleCurrentPost: PropTypes.func,
	onClickIcon: PropTypes.func,
	children:PropTypes.any
  }
export default CardListOrder
