/*Создаем контекст
Итак, первая задача, с которой мы сталкиваемся – как передавать сообщение о событии onClick другому компоненту? Конечно, мы можем в родительском компоненте хранить статус isMenuOpen и передавать callback дочернему элементу. Но по-моему гораздо проще и логичнее использовать контекст для этих целей.

Контект будет передавать два значения:

Булевую переменную isMenuOpen, для информирования компонентов о статусе.
Функцию toggleMenuMode для переключения статуса менюОткрыто/менюЗакрыто.
Создадим файл src/context/navState.js со следующим содержанием:*/ 

import React, { createContext, useState } from 'react';
import PropTypes from 'prop-types';

export const MenuContext = createContext({
  isMenuOpen: true,
  toggleMenu: () => {},
});

const NavState = ({ children }) => {
  const [isMenuOpen, toggleMenu] = useState(false);

  function toggleMenuMode() {
    toggleMenu(!isMenuOpen);
  }

  return (
    <MenuContext.Provider value={{ isMenuOpen, toggleMenuMode }}>{children}</MenuContext.Provider>
  );
};

NavState.propTypes = {
  children: PropTypes.node.isRequired,
};

export default NavState;